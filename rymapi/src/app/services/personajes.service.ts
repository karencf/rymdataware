import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { tap } from 'rxjs/operators';
import { FormControl, FormGroup } from '@angular/forms';
import { Personajes } from '../models/personaje.model';


@Injectable({
    providedIn: 'root'
})
export class PersonajeService {

    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    constructor(private httpClient: HttpClient) {
    }


    getPersonajes(): Observable<Personajes> {
        
        let apiURL = 'https://rickandmortyapi.com/api/character';
        return this.httpClient.get<Personajes>(apiURL,).pipe(tap(
            (res: Personajes) => {
                console.log("Entra aquí")
                return res;
            }
        ));
    }

}
